.. index::
   ! meta-infos

.. _doc_meta_infos:

=====================
Meta
=====================

.. seealso::

   - https://cnt-f.gitlab.io/meta/





Inspiration : sphinx-blogging de Chris Chris Holdgraf
========================================================

.. seealso::

   - https://predictablynoisy.com/posts/2020/sphinx-blogging/
   - https://gdevops.gitlab.io/tuto_ablog/posts/2020/10/10/sphinx_blogging.html
   - https://gdevops.gitlab.io/tuto_ablog/
   - https://gdevops.gitlab.io/tuto_documentation/


Gitlab project
================

.. seealso::

   - https://gitlab.com/cnt-f/meta


Issues
--------

.. seealso::

   - https://gitlab.com/cnt-f/meta/-/boards


Pipelines
------------

.. seealso::

   - https://gitlab.com/cnt-f/meta/-/pipelines


Sphinx theme : **sphinx_book_theme**
============================================

.. seealso::

   - https://gdevops.gitlab.io/tuto_documentation/doc_generators/sphinx/themes/sphinx_book_theme/sphinx_book_theme.html


::

    liste_full = [
        "globaltoc.html",
    html_theme = "sphinx_book_theme"
    copyright = f"2011-{now.year}, , Creative Commons CC BY-NC-SA 3.0. Built with sphinx {sphinx.__version__} Python {platform.python_version()} {html_theme}"


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
