
.. index::
   ! International

.. _groupe_international:

======================
International
======================

.. seealso::

   - https://framagit.org/international



cnt
====

.. seealso::

   - https://international.frama.io/cnt/
   - https://framagit.org/international/cnt
   - https://framagit.org/international/cnt/-/boards
