
.. index::
   ! Congrès Confédéraux
   ! CNT-F

.. _groupe_cnt_f:

====================
CNT-F
====================

.. seealso::

   - https://gitlab.com/cnt-f




Statuts
====================

.. seealso::

   - https://cnt-f.gitlab.io/statuts/
   - https://gitlab.com/cnt-f/statuts
   - https://gitlab.com/cnt-f/statuts/-/boards


Congrès confédéraux
====================

.. seealso::

   - https://cnt-f.gitlab.io/congres_confederaux
   - https://gitlab.com/cnt-f/congres_confederaux
   - https://gitlab.com/cnt-f/congres_confederaux/-/boards


Commissions
===============

.. seealso::

   - https://cnt-f.gitlab.io/commissions
   - https://gitlab.com/cnt-f/commissions
   - https://gitlab.com/cnt-f/commissions/-/boards


Syndicats
==========

.. seealso::

   - https://cnt-f.gitlab.io/syndicats/
   - https://gitlab.com/cnt-f/syndicats
   - https://gitlab.com/cnt-f/syndicats/-/boards


Ile de France
================

.. seealso::

   - https://cnt-f.gitlab.io/iledefrance
   - https://gitlab.com/cnt-f/iledefrance
   - https://gitlab.com/cnt-f/iledefrance/-/boards


Rhône-Alpes
==============

.. seealso::

   - https://cnt-f.gitlab.io/rhone_alpes
   - https://gitlab.com/cnt-f/rhone_alpes
   - https://gitlab.com/cnt-f/rhone_alpes/-/boards


livret_accueil
=================

.. seealso::

   - https://cnt-f.gitlab.io/livret_accueil//
   - https://gitlab.com/cnt-f/livret_accueil
   - https://gitlab.com/cnt-f/livret_accueil/-/boards
