
.. index::
   ! Informatique
   ! Fediverse
   ! Onestlatech

.. _informatique:

======================
Informatique
======================




Fediverse, nextcloud
========================

.. seealso::

   - :ref:`tuto_fediverse:tuto_fediverse`


Outils de communication/gestion de projets
===============================================

.. seealso::

   - :ref:`tuto_project:project_communication`


Onestlatech
==============

.. seealso::

   - :ref:`onestlatech`
