
.. index::
   ! France

.. _groupe_france:

======================
France
======================

Non au fascisme
====================

- https://gassr.gitlab.io/nonaufascisme


Combattre l'antisémitisme
==========================

- https://gassr.gitlab.io/combattrelantisemitisme/


FA Grenoble
==============

- https://gassr.gitlab.io/fagrenoble/
