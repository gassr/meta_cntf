
.. index::
   ! Juridique

.. _juridique:

======================
Juridique
======================

.. seealso::

   - https://france1.frama.io/juridique/



Juridique
============

.. seealso::

   - https://france1.frama.io/juridique/
   - https://framagit.org/france1/juridique
   - https://framagit.org/france1/juridique/-/boards
